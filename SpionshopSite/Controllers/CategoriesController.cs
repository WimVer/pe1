﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using SpionshopLib;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using SpionshopSite.Models;

namespace SpionshopSite.Controllers
{
    public class CategoriesController : ApiController
    {
        private SpionshopEntities db = new SpionshopEntities();

        // GET: api/Categories
        public IQueryable<CategorieDTO> GetCategories()
        {
            Mapper.Initialize(cfg => cfg.CreateMap<Categorie, CategorieDTO>());
            var categories = db.Categories.ProjectTo<CategorieDTO>();
            return categories;
        }

        // GET: api/Categories/5
        [ResponseType(typeof(CategorieDTO))]
        public async Task<IHttpActionResult> GetCategorie(short id)
        {
            Mapper.Initialize(cfg => cfg.CreateMap<Categorie, CategorieDTO>());
            var categorie = await db.Categories.ProjectTo<CategorieDTO>().SingleOrDefaultAsync(c => c.Cat_id == id);
            if (categorie == null)
            {
                return NotFound();
            }

            return Ok(categorie);
        }

        // PUT: api/Categories/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutCategorie(short id, CategorieDTO categorieDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Mapper.Initialize(cfg => cfg.CreateMap<CategorieDTO, Categorie>());
            Categorie categorie = Mapper.Map<Categorie>(categorieDTO);
            db.Set<Categorie>().Attach(categorie);
            db.Entry(categorie).State = EntityState.Modified;
            await db.SaveChangesAsync();
            return Ok(categorieDTO);
        }

        // POST: api/Categories
        [ResponseType(typeof(CategorieDTO))]
        public async Task<IHttpActionResult> PostCategorie(CategorieDTO categorieDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Mapper.Initialize(cfg => cfg.CreateMap<CategorieDTO, Categorie>());
            Categorie categorie = Mapper.Map<Categorie>(categorieDTO);
            db.Categories.Add(categorie);
            await db.SaveChangesAsync();
            categorieDTO.Cat_id = categorie.Cat_id;
            return Ok(categorieDTO);
        }


        // DELETE: api/Categories/5
        [ResponseType(typeof(Categorie))]
        public async Task<IHttpActionResult> DeleteCategorie(short id)
        {
            Categorie categorie = await db.Categories.FindAsync(id);
            if (categorie == null)
            {
                return NotFound();
            }

            db.Categories.Remove(categorie);
            await db.SaveChangesAsync();

            return Ok(categorie);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CategorieExists(short id)
        {
            return db.Categories.Count(e => e.Cat_id == id) > 0;
        }
    }
}