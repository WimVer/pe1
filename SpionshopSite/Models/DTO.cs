﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SpionshopSite.Models
{
        public class CategorieDTO
        {
            public short Cat_id { get; set; }
            public string Categorie1 { get; set; }
        }

        public class ArtikelDTO
        {
            public short Artikel_id { get; set; }
            public string Artikel1 { get; set; }
            public string CategorieCategorie1 { get; set; }

        }

        public class ArtikelDetailDTO
        {
            public short Artikel_id { get; set; }
            public short Cat_id { get; set; }
            public string CategorieCategorie1 { get; set; }
            public string Artikel1 { get; set; }
            public string Omschrijving { get; set; }
            public Nullable<decimal> Verkoopprijs { get; set; }
            public Nullable<short> Instock { get; set; }
        }
}
